`py -m venv venv`

`pip install django`

`django-admin startproject mysite`

`python .\manage.py startapp polls`

`py .\manage.py runserver`

Migrate installed applications databases

`python manage.py migrate`

Mike a migration file. Can be committed to vcs.

`python .\manage.py makemigrations polls`

Generate sql needed for migration

`python manage.py sqlmigrate polls 0001`

Do migration

`python manage.py migrate`

